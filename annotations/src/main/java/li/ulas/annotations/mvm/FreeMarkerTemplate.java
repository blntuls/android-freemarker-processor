package li.ulas.annotations.mvm;

/**
 * Created by bul on 11.03.16.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by bul on 12.03.16.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface FreeMarkerTemplate {
	String templateFile();

	TemplateDirectories templateLocation() default TemplateDirectories.PROJECT;

	TemplateDirectories outputLocation() default TemplateDirectories.PROJECT;

	String templateDir() default "freemarker";

	String outputDir() default "";

	Class<?> valueConverter();

}
