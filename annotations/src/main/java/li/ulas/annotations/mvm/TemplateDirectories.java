package li.ulas.annotations.mvm;

/**
 * Created by bul on 14.03.16.
 */
public enum TemplateDirectories {
	PROJECT("@PROJECT_ROOT"), ABSOULUTE("");

	private final String name;

	TemplateDirectories(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
