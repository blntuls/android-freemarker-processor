package li.ulas.android.templating.freemarker.model;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;

/**
 * Created by bul on 31.03.16.
 */
public class MethodSpecWrapper {
	private final MethodSpec spec;

	public MethodSpecWrapper(MethodSpec spec) {

		this.spec = spec;
	}

	public MethodSpec getSpec() {
		return spec;
	}

	public CodeBlock getCode() {
		return spec.code;
	}

	public String getCodeAsString() {
		return spec.code.toString();
	}

	@Override
	public String toString() {
		return spec.toString();
	}
}

