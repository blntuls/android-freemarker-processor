package li.ulas.android.templating.freemarker.model;

import com.google.common.base.CaseFormat;

import java.util.List;

/**
 * Created by bul on 13.03.16.
 */
public class FreeMarkerModel {

	private ModelClazz modelClazz;
	private String dtoInstanceVarName;
	private Clazz valueConverter;

	public void init(String dtoInstanceVarName) {
		this.dtoInstanceVarName = dtoInstanceVarName;
	}

	public void setDtoInstanceVarName(String dtoInstanceVarName) {
		this.dtoInstanceVarName = dtoInstanceVarName;
	}

	public ModelClazz getModelClazz() {
		return modelClazz;
	}

	public void setModelClazz(ModelClazz modelClazz) {
		this.modelClazz = modelClazz;
		this.modelClazz.initialize();
	}

	public String getDtoInstanceVarName() {
		return dtoInstanceVarName;
	}


	public String delegateMethods() {
		StringBuilder stringBuilder = new StringBuilder();
		List<Method> methods = modelClazz.methods;
		for (Method dm : methods) {
			dm.setValueConverter(this.valueConverter);
			stringBuilder.append(dm.delegateString(dtoInstanceVarName)).append("\n");

		}
		return stringBuilder.toString();
	}

	public void setValueConverter(Clazz valueConverter) {
		this.valueConverter = valueConverter;
	}

	public Clazz getValueConverter() {
		return valueConverter;
	}

	public void prepareToOutput() {
		List<Method> methods = modelClazz.methods;
		for (Method dm : methods) {
			dm.setValueConverter(this.valueConverter);
		}
	}

	public String camelCaseToSnake(String camelCase) {
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, camelCase);
	}
	public String layoutNameToJavaClassName(String layoutName) {
		return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, layoutName);
	}
}
