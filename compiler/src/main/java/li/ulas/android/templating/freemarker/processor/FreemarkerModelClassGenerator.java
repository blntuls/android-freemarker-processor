package li.ulas.android.templating.freemarker.processor;

import com.google.auto.service.AutoService;
import com.google.common.base.Strings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import li.ulas.android.templating.freemarker.FreemarkerHelper;
import li.ulas.android.templating.freemarker.model.FreeMarkerModel;
import li.ulas.android.templating.freemarker.model.FreeMarkerModelCustomProcessor;
import li.ulas.android.templating.freemarker.model.ModelClazz;
import li.ulas.android.templating.freemarker.parser.ViewModelParser;
import li.ulas.annotations.mvm.FreeMarkerTemplate;
import li.ulas.annotations.mvm.TemplateDirectories;

import static java.util.Collections.singleton;
import static javax.lang.model.SourceVersion.latestSupported;
import static javax.tools.Diagnostic.Kind.ERROR;

@AutoService(Processor.class)
public class FreemarkerModelClassGenerator extends AbstractProcessor {

	/**
	 * Annotation Processor Option
	 */
	private static final String PROJECT_ROOT = "projectRoot";
	private static final String CUSTOM_MODEL_PROCESSOR = "customModelProcessor";
	private static final String ANNOTATION = "@" + FreeMarkerTemplate.class.getSimpleName();
	public static final String CUSTOM_FILES_REGEX = "(?:\\s|\r|\n)+(@File=.+)(?:\\s|\r|\n)";
	public static final String DEFAULT_FILE_KEY = "DEFAULT";

	private Messager messager;
	private FreeMarkerModelCustomProcessor postProcessor = null;


	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		messager = processingEnv.getMessager();
		String postProcessor = processingEnv.getOptions().get(CUSTOM_MODEL_PROCESSOR);
		if (Strings.isNullOrEmpty(postProcessor)) {
			return;
		}
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			Class<FreeMarkerModelCustomProcessor> clazz = (Class<FreeMarkerModelCustomProcessor>) classLoader.loadClass(postProcessor);
			this.postProcessor = clazz.newInstance();
		} catch (ClassNotFoundException | InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return singleton(FreeMarkerTemplate.class.getCanonicalName());
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return latestSupported();
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {


		ViewModelParser extractor = new ViewModelParser();
		for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(FreeMarkerTemplate.class)) {
			TypeElement annotatedClass = (TypeElement) annotatedElement;

			FreeMarkerTemplate annotation = annotatedClass.getAnnotation(FreeMarkerTemplate.class);
			String templateName = annotation.templateFile();
			try {
				ModelClazz modelClazz = extractor.extract(annotatedClass);
				FreeMarkerModel freeMarkerModel = new FreeMarkerModel();
				freeMarkerModel.setModelClazz(modelClazz);
				TypeMirror value = null;
				try {
					annotation.valueConverter();
				} catch (MirroredTypeException mte) {
					value = mte.getTypeMirror();
				}

				freeMarkerModel.setValueConverter(extractor.extractClazzFromType(value));
				if (postProcessor != null) {
					freeMarkerModel = postProcessor.process(freeMarkerModel);
				}
				freeMarkerModel.prepareToOutput();
				OutputInfo outInfo = initializeFreemarkerAndOutput(annotatedClass);
				Writer stringWriter = new StringWriter();
				Template temp = FreemarkerHelper.cfg.getTemplate(templateName);
				if (outInfo != null) {
					temp.process(freeMarkerModel, stringWriter);
					stringWriter.flush();
					String s = stringWriter.toString();
					Map<String, String> outputFiles = determinateFilesToWrite(s);
					if (outputFiles.isEmpty()) {
						outInfo.writer.write(s);
					} else {
						String customOutputs = "";
						String defaultFileContent = "";
						for (String fileName : outputFiles.keySet()) {
							if (DEFAULT_FILE_KEY.equals(fileName)) {
								defaultFileContent = outputFiles.get(fileName);
							} else {
								writeToFile(new File(outInfo.outDir, fileName), outputFiles.get(fileName));
							}
							customOutputs = fileName + customOutputs;
						}
						outInfo.writer.write(customOutputs+ "\n"+defaultFileContent);
					}

				} else {
					String message = "No output writer, please check output values for annotation. You can use System.out as default output if you remove output values from your annotation ";
					System.err.println("\nERROR:" + message + "\n\n\n");
					messager.printMessage(ERROR, message, annotatedClass);
				}
			} catch (IOException | TemplateException e) {
				String message = "Template dir " + annotation.templateDir() + " or template file does not exist. Please set  templateDir='my freemarker templates ' in your annotation " + templateName + " not loaded";
				System.err.println("\nERROR:" + message + "\n\n\n");
				messager.printMessage(ERROR, message, annotatedClass);
			}
		}


		return true;
	}

	private void writeToFile(File file, String content) {
		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(content);
			fileWriter.flush();
			fileWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private Map<String, String> determinateFilesToWrite(String content) {
		HashMap<String, String> fileStringHashMap = new HashMap<>();

		Pattern datePatt = Pattern.compile(CUSTOM_FILES_REGEX);
		Matcher matcher = datePatt.matcher(content);
		List<String> files = new ArrayList<>();
		while (matcher.find()) {
			files.add(matcher.group());
		}

		String[] split = content.split(CUSTOM_FILES_REGEX);
		int contentFromIndex = 0;
		if (split.length > files.size()) {
			fileStringHashMap.put(DEFAULT_FILE_KEY, split[0]);
			contentFromIndex = 1;
		}

		for (int i = 0; i < files.size(); i++) {
			fileStringHashMap.put(files.get(i).replaceAll("@", "").replaceAll("File=", "").trim(), split[contentFromIndex]);
			contentFromIndex++;

		}

		return fileStringHashMap;
	}

	private class OutputInfo {
		OutputStreamWriter writer;
		File outDir;
		File outFile;

		private OutputInfo(Builder builder) {
			outDir = builder.outDir;
			writer = builder.writer;
			outFile = builder.outFile;
		}


	}

	private OutputInfo initializeFreemarkerAndOutput(TypeElement annotatedClass) {
		String projectDir = processingEnv.getOptions().get(PROJECT_ROOT);
		FreeMarkerTemplate annotation = annotatedClass.getAnnotation(FreeMarkerTemplate.class);
		String outputDir = annotation.outputDir();
		String templateName = annotation.templateFile();
		try {
			String templateDir = annotation.templateDir();
			if (annotation.templateLocation() == TemplateDirectories.PROJECT) {
				templateDir = projectDir + "/" + templateDir;
			}
			if (!Strings.isNullOrEmpty(outputDir) && annotation.outputLocation() == TemplateDirectories.PROJECT) {
				outputDir = projectDir + "/" + outputDir;
			}
			FreemarkerHelper.cfg.setDirectoryForTemplateLoading(new File(templateDir));

			if (Strings.isNullOrEmpty(outputDir)) {
				return new Builder().writer(new OutputStreamWriter(System.out)).build();
			}
			File file = new File(outputDir);
			if (!file.exists()) {
				boolean created = false;
				if (file.isDirectory()) {
					created = file.mkdirs();
				} else {

					File parentFile = file.getParentFile();
					if (!parentFile.exists()) {
						created = parentFile.mkdirs();

					} else {
						file = new File(parentFile, file.getName());
						created = true;
					}
				}
				if (!created) {
					String message = "Output directory cannot be created " + outputDir + ".\n You have to define a valid folder for file outputs";
					System.err.println("\nERROR:" + message + "\n\n\n");
					messager.printMessage(ERROR, outputDir);

				}
			}
			if (file.isDirectory()) {
				File outFile = new File(file, templateName + ".txt");
				return new Builder().writer(new FileWriter(outFile)).outDir(file).outFile(outFile).build();
			} else {
				return new Builder().writer(new FileWriter(file)).outDir(file.getParentFile()).outFile(file).build();
			}

		} catch (IOException e) {
			e.printStackTrace();
			String message = "Template dir " + annotation.templateDir() + " or template file does not exist. Please set  tin your annotation " + templateName + " not loaded";
			System.err.println("\nERROR:" + message + "\n\n\n");
			messager.printMessage(ERROR, message, annotatedClass);
		}
		return null;
	}


	private final class Builder {
		private File outDir;
		private OutputStreamWriter writer;
		private File outFile;

		public Builder() {
		}

		public Builder outDir(File val) {
			outDir = val;
			return this;
		}

		public Builder writer(OutputStreamWriter val) {
			writer = val;
			return this;
		}

		public Builder outFile(File val) {
			outFile = val;
			return this;
		}

		public OutputInfo build() {
			return new OutputInfo(this);
		}
	}
}

