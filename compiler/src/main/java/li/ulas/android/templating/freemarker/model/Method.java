
package li.ulas.android.templating.freemarker.model;

import com.squareup.javapoet.MethodSpec;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Modifier;

import li.ulas.android.templating.freemarker.parser.ParserUtils;

public class Method {
	public static final int GETTER = 1;
	public static final int SETTER = 2;
	public static final int OTHER = 3;
	public Clazz returnedType;
	public boolean isStatic;
	public boolean isAccessor;
	public int type = OTHER;
	public String name;
	private String propertyName;
	public List<Argument> arguments = new ArrayList<>();
	private Clazz valueConverter;
	private Argument argument;

	private static final String CONVERT_TO_TEMPLATE = "convert%sToString";
	private static final String CONVERT_FROM_TEMPLATE = "get%sFromString";

	public String delegateString(String delName) {
		MethodSpecWrapper mSpec = delegateMethod(delName);
		return mSpec != null ? mSpec.toString() : "";
	}


	public MethodSpecWrapper delegateMethod(String delName) {
		String stringType = "String";
		MethodSpecWrapper mSpec = null;

		if (isGetter()) {
			MethodSpec.Builder plainGetter = MethodSpec.methodBuilder(name).addModifiers(Modifier.PUBLIC).returns(String.class);

			String returnClassName = returnedType.getClassName();
			if (!stringType.equalsIgnoreCase(returnClassName)) {
				plainGetter.addStatement("return $N.$N($N.$N())", valueConverter.getClassName(), String.format(CONVERT_TO_TEMPLATE, ParserUtils.firstToUpperCase(returnClassName)), delName, name);
			} else {
				plainGetter.addStatement("return $N.$N()", delName, name).build();
			}
			mSpec = new MethodSpecWrapper(plainGetter.build());

		} else if (isSetter()) {
			MethodSpec.Builder plainSetter = MethodSpec.methodBuilder(name).addModifiers(Modifier.PUBLIC).returns(void.class);
			this.argument = arguments.get(0);
			System.out.println(name);
			String argName = getArgument().getName();
			String argClazzName = argument.type.getClassName();
			plainSetter.addParameter(String.class, argument.name);
			if (!argClazzName.equalsIgnoreCase(stringType)) {
				plainSetter.addStatement("$N.$N($N.$N($N))", delName, this.name, valueConverter.getClassName(), String.format(CONVERT_FROM_TEMPLATE, ParserUtils.firstToUpperCase(argClazzName)), argName);
			} else {
				plainSetter.addStatement("$N.$N($N)", delName, name, argName);
			}
			mSpec = new MethodSpecWrapper(plainSetter.build());

		}

		return mSpec;
	}


	public boolean isSetter() {
		return type == SETTER;
	}

	public boolean isGetter() {
		return type == GETTER;
	}

	public void setValueConverter(Clazz valueConverter) {
		this.valueConverter = valueConverter;
	}

	public Argument getArgument() {
		return argument == null ? arguments.get(0) : argument;
	}

	public Clazz getValueConverter() {
		return valueConverter;
	}

	public boolean isAccessor() {
		return isAccessor;
	}

	public void setAccessor(boolean accessor) {
		isAccessor = accessor;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public void setStatic(boolean aStatic) {
		isStatic = aStatic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Clazz getReturnedType() {
		return returnedType;
	}

	public void setReturnedType(Clazz returnedType) {
		this.returnedType = returnedType;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<Argument> getArguments() {
		return arguments;
	}

	public void setArguments(List<Argument> arguments) {
		this.arguments = arguments;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Method{");
		sb.append("argument=").append(argument);
		sb.append(", returnedType=").append(returnedType);
		sb.append(", isStatic=").append(isStatic);
		sb.append(", isAccessor=").append(isAccessor);
		sb.append(", type=").append(type);
		sb.append(", name='").append(name).append('\'');
		sb.append(", propertyName='").append(propertyName).append('\'');
		sb.append(", arguments=").append(arguments);
		sb.append(", valueConverter=").append(valueConverter);
		sb.append(", setter=").append(isSetter());
		sb.append(", getter=").append(isGetter());
		sb.append(", accessor=").append(isAccessor());
		sb.append(", static=").append(isStatic());
		sb.append('}');
		return sb.toString();
	}
}
