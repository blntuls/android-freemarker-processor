package li.ulas.android.templating.freemarker.model;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

public class Clazz {
	private final String packageName;
	private final String className;
	private boolean primitive;
	private TypeMirror type;
	private boolean array;

	public Clazz(String packageName, String className) {
		this.packageName = packageName;
		this.className = className;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getClassName() {
		return className;
	}

	public String getConvertToStringStmt() {
		return "--";
	}

	public boolean isPrimitive() {
		return primitive;
	}

	public void setPrimitive(boolean primitive) {
		this.primitive = primitive;
	}

	public String getCannonicalName() {
		if (!getPackageName().isEmpty()) {
			return String.format("%s.%s", getPackageName(), getClassName());
		} else {
			return getClassName();
		}
	}

	@Override
	public String toString() {
		return String.format("%s.%s", packageName, className);
	}

	public void setType(TypeMirror type) {
		this.type = type;
	}

	public TypeMirror getType() {
		return type;
	}

	public boolean isArray() {
		return type.getKind().equals(TypeKind.ARRAY);
	}
}
