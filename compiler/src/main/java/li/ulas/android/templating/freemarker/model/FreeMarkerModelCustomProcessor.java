package li.ulas.android.templating.freemarker.model;

/**
 * Created by bul on 27.03.16.
 */
public interface FreeMarkerModelCustomProcessor {
	FreeMarkerModel process(FreeMarkerModel in);
}
