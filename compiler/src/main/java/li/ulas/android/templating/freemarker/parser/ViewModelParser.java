
package li.ulas.android.templating.freemarker.parser;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

import li.ulas.android.templating.freemarker.model.Argument;
import li.ulas.android.templating.freemarker.model.Clazz;
import li.ulas.android.templating.freemarker.model.Method;
import li.ulas.android.templating.freemarker.model.ModelClazz;

import static li.ulas.android.templating.freemarker.parser.ParserUtils.*;

public final class ViewModelParser {


	public Clazz extractClass(Element element) {
		PackageElement packageElement = (PackageElement) element.getEnclosingElement();
		String className = element.getSimpleName().toString();
		String packageName = packageElement.getQualifiedName().toString();

		return new Clazz(packageName, className);
	}

	public ModelClazz extract(Element dtoClazz) {
		ModelClazz modelClazz = new ModelClazz();
		modelClazz.clazz = extractClass(dtoClazz);
		modelClazz.constructor = processConstructors(modelClazz.clazz.getClassName(), dtoClazz);
		modelClazz.methods = extractMethods(dtoClazz, modelClazz.getters, modelClazz.setters);
		return modelClazz;
	}

	private Method processConstructors(String className, Element element) {
		List<Method> constructors = new ArrayList<>();
		for (Element item : element.getEnclosedElements()) {
			if (item instanceof ExecutableElement) {
				ExecutableElement methodElement = (ExecutableElement) item;
				if (isAConstructor(methodElement) && isAPublicMethod(methodElement)) {
					Method method = new Method();
					method.returnedType = extractClazzFromType(methodElement.getReturnType());
					method.isStatic = isAStaticMethod(methodElement);
					method.name = methodElement.getSimpleName().toString();
					method.arguments = extractMethodArguments(methodElement);
					method.name = className;
					constructors.add(method);
				}
			}
		}
		return constructors.get(0);
	}

	private List<Method> extractMethods(Element element, List<Method> getters, List<Method> setter) {
		List<Method> result = new ArrayList<>();
		for (Element item : element.getEnclosedElements()) {
			if (item instanceof ExecutableElement) {
				ExecutableElement methodElement = (ExecutableElement) item;
				if (!isAConstructor(methodElement) && isAPublicMethod(methodElement)) {
					Method method = new Method();
					method.returnedType = extractClazzFromType(methodElement.getReturnType());
					method.isStatic = isAStaticMethod(methodElement);
					int methodType = getMethodType(methodElement);
					method.type = methodType;
					method.isAccessor = method.type != Method.OTHER;
					method.name = methodElement.getSimpleName().toString();
					method.arguments = extractMethodArguments(methodElement);
					if (method.isAccessor) {
						if (methodType == Method.GETTER) {
							getters.add(method);
						} else {
							setter.add(method);
						}
					}
					result.add(method);
				}
			}
		}
		return result;
	}

	private int getMethodType(ExecutableElement methodElement) {
		if (isJavaBeanGetter(methodElement)) {
			return Method.GETTER;
		} else if (isJavaBeanSetter(methodElement)) {
			return Method.SETTER;
		}
		return Method.OTHER;
	}

	private List<Argument> extractMethodArguments(ExecutableElement element) {
		List<Argument> result = new ArrayList<>();
		for (VariableElement item : element.getParameters()) {
			Argument argument = new Argument();
			argument.name = item.getSimpleName().toString();
			argument.type = extractClazzFromType(item.asType());
			result.add(argument);
		}
		return result;
	}

	public Clazz extractClazzFromType(TypeMirror type) {

		String packageName = "";
		String className;

		String typeCannonicalName = type.toString();
		int firstMinor = typeCannonicalName.indexOf('<');
		int lastPoint = typeCannonicalName.lastIndexOf('.');
		if (firstMinor > -1) {
			String temporaryExtraction = typeCannonicalName.substring(0, firstMinor);
			packageName = typeCannonicalName.substring(0, temporaryExtraction.lastIndexOf('.'));
			className = typeCannonicalName.substring(packageName.length() + 1);
		} else {
			if (lastPoint > -1) {
				packageName = typeCannonicalName.substring(0, lastPoint);
				className = typeCannonicalName.substring(lastPoint + 1);
			} else {
				className = type.toString();
			}
		}
		Clazz clazz = new Clazz(packageName, className);
		clazz.setPrimitive(type.getKind().isPrimitive());
		clazz.setType(type);
		return clazz;
	}

}
