
package li.ulas.android.templating.freemarker.model;

public class Argument {
	public Clazz type;
	public String name;

	@Override
	public String toString() {
		return String.format("%s, %s", name, type);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(Clazz type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Clazz getType() {
		return type;
	}
}
