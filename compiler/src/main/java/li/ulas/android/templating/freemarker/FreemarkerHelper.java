package li.ulas.android.templating.freemarker;

import freemarker.template.Configuration;

/**
 * Created by bul on 03.03.16.
 */
public class FreemarkerHelper {
	public static Configuration cfg;

	static {

		cfg = new Configuration(Configuration.VERSION_2_3_22);
		cfg.setDefaultEncoding("UTF-8");
	}
}
