
package li.ulas.android.templating.freemarker.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static li.ulas.android.templating.freemarker.parser.ParserUtils.convertToPropertyName;

public class ModelClazz {
	public Clazz clazz;
	public Method constructor;
	public List<Method> methods = new ArrayList<>();
	public List<Method> getters = new ArrayList<>();
	public List<Method> setters = new ArrayList<>();
	public Collection<String> propertyConstants = new HashSet<>();
	boolean isInitialized = false;
	private Collection<String> propertyNames = new HashSet<>();
	private Map<String, Method> gettersByProperty = new HashMap<>();
	private Map<String, Method> settersByProperty = new HashMap<>();
	private Map<String, String> propertyNameToConstantName = new HashMap<>();
	private Map<String, String> constantNameToPropertyName = new HashMap<>();
	private String PROP_PREFIX = "PROPERTY_NAME_";

	private void init() {
		if (isInitialized) {
			return;
		}

		for (Method m : methods) {
			String name = convertToPropertyName(m.name);
			boolean isAccessor = m.isAccessor();
			if (!isAccessor) {
				continue;
			}
			m.setPropertyName(name);

			String constName = PROP_PREFIX + name.toUpperCase();
			propertyConstants.add(constName);
			constantNameToPropertyName.put(constName, name);
			propertyNameToConstantName.put(name, constName);
			if (m.isGetter()) {
				gettersByProperty.put(name, m);
			} else if (m.isSetter()) {
				settersByProperty.put(name, m);
			}
			propertyNames.add(name);
		}
	}

	public String getClassName() {
		return clazz.getClassName();
	}


	public Collection<String> getPropertyNames() {
		init();
		return propertyNames;
	}

	public Method getGetterMethodForProperty(String name) {
		init();
		return gettersByProperty.get(name);

	}

	public Method getSetterMethodForProperty(String name) {
		init();
		return settersByProperty.get(name);
	}

	public Method getGetterMethodForPropertyConstant(String constantPropName) {
		init();
		return gettersByProperty.get(constantNameToPropertyName.get(constantPropName));

	}

	public String getConstantForPropertyName(String constantPropName) {
		init();
		return propertyNameToConstantName.get(constantPropName);

	}

	public String getPropertyForConstant(String constantName) {
		init();
		return constantNameToPropertyName.get(constantName);

	}

	public Method getSetterMethodForPropertyConstant(String constantPropName) {
		init();
		return settersByProperty.get(constantNameToPropertyName.get(constantPropName));
	}

	public Collection<String> getPropertyNameConstants() {
		init();
		return constantNameToPropertyName.keySet();
	}

	public Collection<String> getPropertyConstants() {
		init();
		return propertyConstants;
	}

	public List<Method> getGetters() {
		init();
		return getters;
	}

	public Method getConstructor() {
		init();
		return constructor;
	}

	public Clazz getClazz() {
		init();
		return clazz;
	}

	public List<Method> getMethods() {
		init();
		return methods;
	}

	public List<Method> getSetters() {
		init();
		return setters;
	}

	public void initialize() {
		init();

	}
}
