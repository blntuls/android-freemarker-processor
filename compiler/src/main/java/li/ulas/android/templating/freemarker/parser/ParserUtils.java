package li.ulas.android.templating.freemarker.parser;

import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import static javax.lang.model.element.Modifier.*;

/**
 * Created by bul on 14.03.16.
 */
public class ParserUtils {

	public static boolean isAConstructor(ExecutableElement methodElement) {
		return methodElement.getSimpleName().toString().equals("<init>");
	}

	public static boolean isAPublicMethod(ExecutableElement methodElement) {
		boolean result;
		Set<Modifier> modifiers = methodElement.getModifiers();
		result = modifiers.contains(Modifier.PUBLIC);
		return result;
	}

	public static boolean isAStaticMethod(ExecutableElement methodElement) {
		boolean result;
		Set<Modifier> modifiers = methodElement.getModifiers();
		result = modifiers.contains(Modifier.STATIC);
		return result;
	}

	/**
	 * Convenience method to return if the provided method is a java bean getter.
	 *
	 * @param method The method
	 * @return Whether it is a java bean getter
	 */
	public static boolean isJavaBeanGetter(ExecutableElement method) {
		String methodName = method.getSimpleName().toString();
		if (method.getKind() == ElementKind.METHOD && method.getParameters().isEmpty()) {
			if (returnsBoolean(method) && methodName.startsWith("is")) {
				return true;
			} else if (methodName.startsWith("get") && !returnsVoid(method)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Convenience method to return if the provided method is a java bean setter.
	 *
	 * @param method The method
	 * @return Whether it is a java bean setter
	 */
	public static boolean isJavaBeanSetter(ExecutableElement method) {
		String methodName = method.getSimpleName().toString();
		return method.getKind() == ElementKind.METHOD && methodName.startsWith("set") &&
				method.getParameters().size() == 1 && returnsVoid(method);
	}

	/**
	 * Convenience method to return if the provided element represents a method (otherwise a field).
	 *
	 * @param elem The element
	 * @return Whether it represents a method
	 */
	public static boolean isMethod(Element elem) {
		return elem != null && ExecutableElement.class.isInstance(elem) && elem.getKind() == ElementKind.METHOD;
	}

	/**
	 * Convenience method returning if the provided method returns void.
	 *
	 * @param method The method
	 * @return Whether it returns void
	 */
	public static boolean returnsVoid(ExecutableElement method) {
		TypeMirror type = method.getReturnType();
		return (type != null && type.getKind() == TypeKind.VOID);
	}

	/**
	 * Convenience method returning if the provided method returns boolean.
	 *
	 * @param method The method
	 * @return Whether it returns boolean
	 */
	public static boolean returnsBoolean(ExecutableElement method) {
		TypeMirror type = method.getReturnType();
		return (type != null && (type.getKind() == TypeKind.BOOLEAN || "java.lang.Boolean".equals(type.toString())));
	}

	/**
	 * Convenience method to return if the provided type is a primitive.
	 *
	 * @param type The type
	 * @return Whether it is a primitive
	 */
	public static boolean typeIsPrimitive(TypeMirror type) {
		TypeKind kind = type.getKind();
		return kind == TypeKind.BOOLEAN || kind == TypeKind.BYTE || kind == TypeKind.CHAR ||
				kind == TypeKind.DOUBLE || kind == TypeKind.FLOAT || kind == TypeKind.INT ||
				kind == TypeKind.LONG || kind == TypeKind.SHORT;
	}


	public static boolean isPublic(TypeElement annotatedClass) {
		return annotatedClass.getModifiers().contains(PUBLIC);
	}

	public static boolean isAbstract(TypeElement annotatedClass) {
		return annotatedClass.getModifiers().contains(ABSTRACT);
	}

	public static String convertToPropertyName(String name) {
		int startIndex = 0;
		if (name.startsWith("set") || name.startsWith("get")) {
			startIndex = 3;
		} else if (name.startsWith("is")) {
			startIndex = 2;
		}
		return new StringBuilder(name.length() - startIndex)
				.append(Character.toLowerCase(name.charAt(startIndex)))
				.append(name.substring(startIndex + 1))
				.toString();
	}

	public static String firstToUpperCase(String name) {
		int startIndex = 0;

		return new StringBuilder(name.length() - startIndex)
				.append(Character.toUpperCase(name.charAt(startIndex)))
				.append(name.substring(startIndex + 1))
				.toString();
	}
}
